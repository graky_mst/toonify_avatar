import datetime
from uuid import uuid4

import sqlalchemy
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import UUID, ARRAY
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class User(Base):
    __tablename__ = "auth_user"
    id = Column(sqlalchemy.Integer, primary_key=True, index=True)
    username = Column(sqlalchemy.String(30), nullable=False, unique=True)
    email = Column(sqlalchemy.String(254), nullable=True, unique=True)
    phone = Column(sqlalchemy.String(128), nullable=True, unique=True)
    password = Column(sqlalchemy.String(128))
    city = Column(sqlalchemy.String(30), nullable=True)
    first_name = Column(sqlalchemy.String(30), nullable=True)
    last_name = Column(sqlalchemy.String(30), nullable=True)
    patronymic = Column(sqlalchemy.String(30), nullable=True)
    date_joined = Column(sqlalchemy.DateTime, default=datetime.datetime.utcnow)
    date_birthday = Column(sqlalchemy.Date, nullable=True)
    date_password_change = Column(sqlalchemy.DateTime, nullable=True)
    is_active = Column(sqlalchemy.Boolean, default=True)
    # Column("avatar", sqlalchemy., default=True),
    sex = Column(sqlalchemy.String(1), nullable=True)
    interests = Column(ARRAY(sqlalchemy.String), default=list)
    about = Column(sqlalchemy.String, nullable=True)
    notification_check_work_email = Column(
        sqlalchemy.Boolean, nullable=True, default=False
    )
    notification_check_work_sys = Column(
        sqlalchemy.Boolean, nullable=True, default=False
    )
    notification_changes_material_email = Column(
        sqlalchemy.Boolean, nullable=True, default=False
    )
    notification_changes_material_sys = Column(
        sqlalchemy.Boolean, nullable=True, default=False
    )
    notification_update_email = Column(sqlalchemy.Boolean, nullable=True, default=False)
    notification_update_sys = Column(sqlalchemy.Boolean, nullable=True, default=False)
    avatar = Column(sqlalchemy.String(100), nullable=True)
    toonified_avatars = relationship("ToonifiedAvatar", back_populates="user")


class ToonifiedAvatar(Base):
    __tablename__ = "toonified"
    uuid = Column(UUID(as_uuid=True), default=uuid4, primary_key=True)
    image = Column(sqlalchemy.String(100), nullable=True)
    user_id = Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("auth_user.id"))
    user = relationship("User", back_populates="toonified_avatars")
