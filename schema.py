import re
import uuid

import jwt
import strawberry
from datetime import datetime, timedelta
from toonifier import toonifier
from projector import projector
from passlib.context import CryptContext
from fastapi_sqlalchemy import db

from gql_types import (
    RequestAvatar,
    TokenAuthType,
    TokenOutput,
    UserProfileOutput,
    ToonifiedAvatarOutput,
)
from models import User, ToonifiedAvatar

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_DAYS = 7

pwd_context = CryptContext(schemes=["sha256_crypt"], deprecated="auto")


def verify_token(info=None, token=None):
    if info:
        authorization = info.context["request"]._headers.get("authorization")
        if authorization:
            token = authorization.split(" ")[1]
        else:
            return {"error": "bad token"}
    if not token:
        return {"error": "bad token"}
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    username = payload.get("username")
    if username:
        return username
    return {"error": "bad token"}


@strawberry.type
class Mutation:
    @strawberry.mutation
    def toonify_avatar(
        self, info, get_toonified_avatar: RequestAvatar
    ) -> ToonifiedAvatarOutput:

        username = verify_token(info=info)
        if type(username) != str:
            raise Exception("Bad token")
        user = db.session.query(User).filter_by(username=username).one_or_none()
        if get_toonified_avatar.use_avatar:
            file = user.avatar
        elif info.context.FILES.get("avatar"):
            file = info.context.FILES.get("avatar")
        else:
            raise Exception("No file")
        projected_avatar = projector(file)
        toonified_avatar = toonifier(projected_avatar)
        if get_toonified_avatar.set_as_avatar:
            user.avatar = file
            db.session.commit()
        toonified_avatar_save = ToonifiedAvatar(
            user_id=user.id, image=toonified_avatar, uuid=uuid.uuid4()
        )
        avatar_uuid = toonified_avatar_save.uuid
        db.session.add(toonified_avatar_save)
        db.session.commit()
        db.session.close()
        return ToonifiedAvatarOutput(image=toonified_avatar, uuid=avatar_uuid)

    @strawberry.mutation
    def token_auth(self, info, token_auth: TokenAuthType) -> TokenOutput:
        login = token_auth.login
        password = token_auth.password
        login = re.match(r"\b[\w.-]+@[\w.-]+.\w{2,4}\b", login) or re.match(
            r"^\+\d{11}$", login
        )
        if not login:
            return TokenOutput(token="", error="Please, enter valid credentials")
        login = login.group(0)
        user = (
            db.session.query(User)
            .filter((User.email == login) | (User.phone == login))
            .first()
        )
        if not user:
            return TokenOutput(token="", error="Please, enter valid credentials")
        check_password = pwd_context.verify(password, user.password)
        if not check_password:
            return TokenOutput(token="", error="Please, enter valid credentials")
        username = user.username
        expire = datetime.utcnow() + timedelta(days=ACCESS_TOKEN_EXPIRE_DAYS)
        to_encode = {"username": username, "exp": expire}
        encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        db.session.close()
        return TokenOutput(token=encoded_jwt, error="")


@strawberry.type
class Query:
    @strawberry.field
    def user_info(self, info) -> UserProfileOutput:
        username = verify_token(info=info)
        if type(username) != str:
            raise Exception("Bad token")
        user = db.session.query(User).filter_by(username=username).first()
        user = user.__dict__
        del user["_sa_instance_state"]
        del user["password"]
        user["uuid"] = str(user["uuid"])
        db.session.close()
        return UserProfileOutput(**user)


schema = strawberry.Schema(mutation=Mutation, query=Query)
