import typing
import strawberry
import datetime


@strawberry.type
class BasicOutput:
    success: bool
    message: str


@strawberry.type
class VerifyOutput:
    username: str
    exp: int


@strawberry.input
class TokenAuthType:
    login: str = strawberry.field(description="Username")
    password: str = strawberry.field(description="Password")


@strawberry.input
class VerifyTokenType:
    token: str = strawberry.field(description="Token")


@strawberry.input
class RequestAvatar:
    set_as_avatar: typing.Optional[bool] = False
    use_avatar: typing.Optional[bool] = False


@strawberry.type
class GeneratedAvatar:
    avatar: str


@strawberry.type
class TokenOutput:
    token: str
    error: str


@strawberry.type
class UserProfileOutput:
    uuid: str
    email: str
    username: str
    city: str
    first_name: str
    last_name: str
    patronymic: str
    date_birthday: str
    interests: typing.List[str]
    about: str
    sex: str
    phone: str
    date_joined: datetime.time
    date_birthday: datetime.date
    date_password_change: datetime.time
    is_active: bool
    notification_check_work_email: bool
    notification_check_work_sys: bool
    notification_changes_material_email: bool
    notification_changes_material_sys: bool
    notification_update_email: bool
    notification_update_sys: bool


@strawberry.type
class ToonifiedAvatarOutput:
    uuid: str
    image: str
